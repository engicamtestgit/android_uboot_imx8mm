#!/bin/bash
export MY_ANDROID=/home/domenico/android_build
export ARCH=arm64
export CROSS_COMPILE=${MY_ANDROID}/prebuilts/gcc/linux-x86/aarch64/aarch64-linux-android-4.9/bin/aarch64-linux-android-
if [ ! -f ${MY_ANDROID}/vendor/nxp-opensource/uboot-imx/.config ]; then
	make imx8mm_eng_android_defconfig
	make -j16
else
	make -j16
fi

cp -v spl/u-boot-spl.bin ../imx-mkimage/iMX8M/
cp -v u-boot-nodtb.bin ../imx-mkimage/iMX8M/
cp -v arch/arm/dts/eng-imx8mm-evk.dtb ../imx-mkimage/iMX8M/
cd ../imx-mkimage/
make SOC=iMX8MM flash_spl_uboot dtbs=eng-imx8mm-evk.dtb

